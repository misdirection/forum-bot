import { createClient, PrivateMessageEvent, GroupMessageEvent, DiscussMessageEvent, Sendable, MessageElem } from 'icqq'
import { changeBlackUidStr, changeBlackWordsStr, changeWhiteWordsStr, init } from './src/init'
import fs from 'fs'
const config = require('./config.json')
export interface IConfig {
  botAccount: number
  myAccount: number
  password: string
  notice: {
    group: number[]
    private: number[]
    graphic: boolean
  }
  interval: number
}

export type EventData = PrivateMessageEvent | GroupMessageEvent | DiscussMessageEvent
;(() => {
  if (!config.botAccount || !config.password) return console.log('未设置机器人账户密码')

  const client = createClient()

  const checkMessage = async (type: string, msg: string, isDel: boolean, data: EventData) => {
    if (!msg) return
    switch (type) {
      case '屏蔽词':
        return changeBlackWordsStr(msg, isDel, data)
      case '白名单':
        return changeWhiteWordsStr(msg, isDel, data)
      case '屏蔽用户':
        return changeBlackUidStr(msg, isDel, data)
      case 'cookie':
        const config = JSON.parse(fs.readFileSync('./config.json', 'utf-8'))
        config.cookie = msg
        fs.writeFileSync('./config.json', JSON.stringify(config), 'utf-8')
        await data.reply('添加 Cookie 成功')
    }
  }
  client.on('system.online', () => {
    init(async (arr) => {
      // arr.reduce((a, b, i) => `${a}${i ? '\n' : ''}${b.content}\n${b.link}`, '')
      const msgArr: Sendable = arr.reduce((a: any, b, i) => {
        a.push(`${i ? '\n' : ''}${b.content}\n`)
        if (b.img) {
          a.push({
            type: 'image',
            file: b.img
          })
        }
        b.link && a.push((b.img ? '\n' : '') + b.link)
        return a
      }, [])

      try {
        for (let key of [config.myAccount, ...config.notice.private]) {
          await client.sendPrivateMsg(key, msgArr).catch(console.log) //私聊
        }
        for (let key of config.notice.group) {
          await client.sendGroupMsg(key, msgArr).catch(console.log)
        }
        return true
      } catch (e) {
        console.log(e)
        return false
      }
    }, config)
  })

  client.on('message', (data: EventData) => {
    if (data.sender.user_id !== +config.myAccount) return
    // const raw_message = data.raw_message.replace(/(?:\[.*?\]\s?)*([\s\S]+)/, (a, b) => b || '').trim()
    const [, isDel, type, msg] = data.raw_message.trim().match(/\!(删除)?(屏蔽词|白名单|屏蔽用户|cookie)\s([\s\S]+)/) || []

    if (!type) return
    if (data.message_type === 'private') {
      if (data.to_id === +config.botAccount) checkMessage(type, msg, !!isDel, data)
    } else if (data.message_type === 'group' && data.atme) {
      checkMessage(type, msg, !!isDel, data)
    }
  })

  client.on('system.login.slider', (e: any) => {
    console.log('输入滑块地址获取的ticket后继续。\n滑块地址:    ' + e.url)
    process.stdin.once('data', (data) => {
      client.submitSlider(data.toString().trim())
    })
  })
  // client.on('system.login.qrcode', (e: any) => {
  //     console.log('扫码完成后回车继续:    ')
  //     process.stdin.once('data', () => {
  //         client.login()
  //     })
  // })

  client.on('system.login.device', (e: any) => {
    console.log('请选择验证方式:(1：短信验证   其他：扫码验证)')
    process.stdin.once('data', (data) => {
      if (data.toString().trim() === '1') {
        client.sendSmsCode()
        console.log('请输入手机收到的短信验证码:')
        process.stdin.once('data', (res) => {
          client.submitSmsCode(res.toString().trim())
        })
      } else {
        console.log('扫码完成后回车继续：' + e.url)
        process.stdin.once('data', () => {
          client.login()
        })
      }
    })
  })
  client.login(config.botAccount, config.password)
})()
