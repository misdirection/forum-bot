# forum-bot

#### 介绍
QQ机器人推送论坛消息


#### 安装教程

1.  git clone https://gitee.com/misdirection/forum-bot.git
2.  cd ./forum-bot
3.  npm i
4.  npm start

#### 使用说明

1.  修改 `config.ts` 中的配置
2.  npm start