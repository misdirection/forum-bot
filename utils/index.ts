import fs from 'fs'

export const read = (path: string, defaultStr = '{}') => {
  let obj = JSON.parse(defaultStr)
  try {
    obj = JSON.parse(fs.readFileSync(path, 'utf-8'))
  } catch (e) {
    fs.appendFileSync(path, defaultStr, 'utf-8')
  }
  return obj
}
export const write = (path: string, data: string) => {
  try {
    fs.writeFileSync(path, data, 'utf-8')
  } catch (e) {
    console.log(`${path}写入文件失败, ${e}`)
  }
}
