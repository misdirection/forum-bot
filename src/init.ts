import axios from 'axios'
import { read, write } from '../utils'
import { EventData, IConfig } from '../index'

// let WhiteRegionStr = '购物心得|福利放送|热点聚焦|杉果|谈天说地|慈善包'
// let WhiteWordsStr = '赠送游戏|秒杀|骨折|嫖|好价|羊毛|临时工|薅|免费|新包|欢乐时光开始|自选包|明星特惠|无门槛|旧包|内容公布|获得全部内容'
// let BlackWordsStr = '非(?:s|S)team|电信|试用|百度|移动|联通|试玩|试用|体验|免费游戏|Beta|测试|行测会员|免费上线|心得|剧透|新闻汇总|(?:a|A)mazon\\s(?:p|P)rime'
export interface ILinkObj {
  link: string
  content: string
  region: string
  uid: string
  time: number
  hasImg: boolean
  img?: string
}
interface IDbData {
  BlackWordsStr: string
  WhiteWordsStr: string
  BlackUserStr: string
}

const saveWord = (msg: string, obj: IDbData, str: keyof IDbData, title: string, isDel: boolean, data: EventData) => {
  let strArr: string[] = []
  let msgArr: string[] = []

  if (isDel) {
    msgArr = msg.split(',').filter((item) => item)
    strArr = obj[str].split('|').filter((item) => item && !msgArr.includes(item))
  } else {
    strArr = obj[str].split('|').filter((item) => item)
    msgArr = msg.split(',').filter((item) => item && !strArr.includes(item))
  }
  if (msgArr.length) {
    data.reply(`${title}: ${msgArr.join(',')} ${isDel ? '删除' : '添加'}成功~`)
    obj[str] = (isDel ? strArr : [...strArr, ...msgArr]).join('|')
    write('./database/db.json', JSON.stringify(obj))
  } else {
    data.reply(`${title}: ${msg} ${isDel ? '未找到' : '已拥有'}~`, data.message_type !== 'private')
  }
}
export const changeBlackWordsStr = (msg: string, isDel: boolean, data: EventData) => {
  const obj = read('./database/db.json')
  saveWord(msg, obj, 'BlackWordsStr', '屏蔽词', isDel, data)
}

export const changeWhiteWordsStr = (msg: string, isDel: boolean, data: EventData) => {
  const obj = read('./database/db.json')
  saveWord(msg, obj, 'WhiteWordsStr', '白名单', isDel, data)
}

export const changeBlackUidStr = (msg: string, isDel: boolean, data: EventData) => {
  const obj = read('./database/db.json')
  saveWord(msg, obj, 'BlackUserStr', '屏蔽用户', isDel, data)
}

export const init = (sendMessage: (arr: Partial<ILinkObj>[]) => Promise<boolean>, config: IConfig) => {
  let isValid: boolean = false
  const fun = async () => {
    const { WhiteRegionStr, WhiteWordsStr, BlackWordsStr, BlackUserStr } = read('./database/db.json', '[]')
    const configJSON = read('./config.json') 

    let allArr: ILinkObj[] = read('./database/link.json', '[]')
    const WhiteRegion = new RegExp(WhiteRegionStr, 'i')
    const WhiteWords = new RegExp(WhiteWordsStr, 'i')
    const BlackWords = new RegExp(BlackWordsStr, 'i')
    const BlackUser = new RegExp(BlackUserStr, 'i')
    const htmlResult = await axios.get<any, { data: string }>('https://keylol.com/forum.php?mod=guide&view=newthread', {
      headers: { cookie: configJSON.cookie || '' }
    }).catch(console.log)

    if (!htmlResult || !htmlResult?.data) return

    if (htmlResult.data.includes('您需要先登录才能继续本操作')) {
      if(isValid) return
      await sendMessage([{ content: 'Cookie 失效' }])
      return (isValid = true)
    }

    isValid = false
    const time = Date.now()

    allArr = allArr.filter((item) => time - item.time < 3600 * 24 * 1000)

    write('./database/link.json', JSON.stringify(allArr))

    const reg = /<th class="common">\s+(?:<em>.*?<\/em>\s)?<a href="([\w-]+)" target="_blank" class="xst"\s>(.*?)<\/a>&nbsp;(?:\s?- (?:<span|\[).*?(?:\]<\/span>\s+|\]))?(<i class="fico-image fic4 fc-p fnmr vm" title="图片附件"><\/i>)?[\s\S]+?<td class="by"><a href="[\w-]+" target="_blank">(.*?)<\/a><\/td>\s+<td class="by">\s+<cite>\s+<a href="suid-\d+" c="\d">(.*?)<\/a><\/cite>/g

    let result,
      arr: ILinkObj[] = []
    while ((result = reg.exec(htmlResult.data))) {
      const [, link, content, hasImg, region, uid] = result
      if (!WhiteRegionStr || !WhiteRegion.test(region)) continue
      if (!WhiteWordsStr || !WhiteWords.test(content)) continue
      if (BlackWordsStr && BlackWords.test(content)) continue
      if (BlackUserStr && BlackUser.test(uid)) continue
      arr.push({
        link: 'https://keylol.com/' + link,
        content,
        region,
        uid,
        hasImg: !!hasImg,
        time
      })
    }
    arr = arr.filter((item) => allArr.every((itm) => itm.link !== item.link))
    if (!arr.length) return console.log('未找到')
    if (config.notice.graphic) {
      for (let obj of arr) {
        if (!obj.hasImg) continue
        const detailResult = await axios.get(obj.link, {
          headers: {
            cookie: configJSON.cookie || ''
          }
        }).catch(console.log)
        if (!detailResult?.data) continue
        const [, img] = detailResult.data.match(/<(?:ignore_js_op|div class="mbn savephotop")>\s+<img.*?zoomfile="(.*?)"/) || []
        if (!img) continue
        obj.img = img
      }
    }
    const flag = await sendMessage(arr)
    if (!flag) return
    allArr.push(...arr)
    write('./database/link.json', JSON.stringify(allArr))
  }
  fun()
  setInterval(fun, (Number.isNaN(Number(config.interval)) || config.interval < 1 ? 1 : config.interval) * 60000)
}
